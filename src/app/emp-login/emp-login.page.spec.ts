import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpLoginPage } from './emp-login.page';

describe('EmpLoginPage', () => {
  let component: EmpLoginPage;
  let fixture: ComponentFixture<EmpLoginPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpLoginPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpLoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
