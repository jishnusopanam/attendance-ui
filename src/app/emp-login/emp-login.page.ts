
import { Component, OnInit } from '@angular/core';
import { BarcodeScanner,BarcodeScannerOptions,BarcodeScanResult } from '@ionic-native/barcode-scanner/ngx';

@Component({
  selector: 'app-emp-login',
  templateUrl: './emp-login.page.html',
  styleUrls: ['./emp-login.page.scss'],
})
export class EmpLoginPage implements OnInit {
  
  options:BarcodeScannerOptions;
  scanResult: BarcodeScanResult;
  scannedData:any={};

 constructor(private scanner: BarcodeScanner) { }
 ionViewWillEnter() {
   this.scan()
 }

  ngOnInit() {
  
}
scan(){
  this.options={
    prompt:"scan your qrcode"
       }
   this.scanner.scan(this.options).then((data)=>{
     this.scannedData = data;

   }, (err) =>{
     console.log("error : ",err);
   })
   
}
}
