import { HomePage } from './home/home.page';
import { NavController, NavPush } from 'ionic-angular';
import { EmpLoginPage } from './emp-login/emp-login.page';
import { AppSettings } from './config/app-settings-debug';

import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy,Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';







@NgModule({
  declarations: [AppComponent,EmpLoginPage],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    
    
 
  ],
  providers: [
    StatusBar,
    SplashScreen,
    BarcodeScanner,
    AppSettings,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Platform,
    HttpClientModule
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
