import { HomePage } from './../home/home.page';
import { Injectable } from "@angular/core";
import { AppSettingsBase } from "../config/app-settings-base";


@Injectable()
export class AppSettings extends AppSettingsBase {
  constructor(public home:HomePage) {
    super();

    /*Base Setting*/
    //this.domainUrl = 'http://52.79.216.67:8080';
    this.integrationServerUrl = "https://pinnpark.com/ext/api";
    this.EmployeeLog = `https://pinnpark.in/emp/api`;
    this.Employee = 'http://localhost:3000/api';
    // this.baseApiUrl = `https://pinnpark.in/wonderla/api`;
    // this.baseApiUrl = `https://pinnpark.in/customer/v2/api`;
    this.baseApiUrl = `https://pinnpark.com/wallet/api`;
    // this.baseApiUrl = `https://pinnpark.com/customer/v3/api`;
    // this.baseApiUrl = `https://pinpark.in/customer/v3/api`;
    // this.baseApiUrl = `http://localhost:3001/api`;
    // this.promoUrl = `http://localhost:3000/api`;
    this.scan = 'https://pinpark.com/contract';
  }

  getBaseApiUrl(): string {
    return this.baseApiUrl;
  }
  scanner(scannedData){
   return '${this.Employee}/Employee/Employee_find/filter/employeeCode?employeeCode=${this.scan}/${scannedData}';
  }
}
  
