import { EmpLoginPage } from './../emp-login/emp-login.page';
import { Component, ViewChild } from '@angular/core';
import { NavController,NavParams } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';






@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private navCtrl: NavController,private navParams: NavParams,public HttpClient:HttpClientModule) {}
  scan(){
      this.navCtrl.push(EmpLoginPage);
      }



   
}
